import * as React from 'react';
import AddOption from './AddOption';
import Header from './Header';
import Action from './Action';
import Options from './Options';
import OptionModal from './OptionModal';

interface State {
    options: string[];
    selectedOption: undefined | string;
    option: string;
}
export default class IndecisionApp<Props> extends React.Component<
    Props,
    State
> {
    state: State = {
        options: [],
        selectedOption: undefined,
        option: '',
    };
    handleClearSelectedOption: VoidFunction = () => {
        this.setState(() => ({ selectedOption: undefined }));
    };
    handleAddOtion: Function = (option: never) => {
        if (!option) {
            return 'enter valid value to add item';
        } else if (this.state.options.indexOf(option) > -1) {
            return 'this option is already exists';
        } else {
            this.setState((prevState: State) => ({
                options: prevState.options.concat([option]),
            }));
        }
    };
    handleDeleteOptions: VoidFunction = () => {
        this.setState(() => ({ options: [] }));
    };
    handleDeleteOption: Function = (option: string) => {
        this.setState((prevState: State) => ({
            options: prevState.options.filter((opt: string) => option !== opt),
        }));
    };
    handlePick: VoidFunction = () => {
        const randomNum: number = Math.floor(
            Math.random() * this.state.options.length
        );
        const option: string = this.state.options[randomNum];
        this.setState(() => ({ selectedOption: option }));
    };
    componentDidMount() {
        try {
            const json: string | null = localStorage.getItem('options');
            const options: string[] | null = JSON.parse(json!);
            if (options) {
                this.setState(() => ({ options }));
            }
        } catch (err) {
            console.log(err);
        }
    }
    componentDidUpdate(prevProps: Props, prevState: State) {
        if (this.state.options.length !== prevState.options.length) {
            const json: string = JSON.stringify(this.state.options);
            localStorage.setItem('options', json);
        }
    }
    render() {
        const subtitle = 'Put your life in the hands of your pc';
        return (
            <div>
                <Header subtitle={subtitle} />
                <div className="container">
                    <Action
                        hasOptions={this.state.options.length > 0}
                        handlePick={this.handlePick}
                    />
                    <div className="widget">
                        <Options
                            options={this.state.options}
                            handleDeleteOptions={this.handleDeleteOptions}
                            handleDeleteOption={this.handleDeleteOption}
                        />
                        <AddOption handleAddOtion={this.handleAddOtion} />
                    </div>
                </div>
                <OptionModal
                    selectedOption={this.state.selectedOption}
                    handleClearSelectedOption={this.handleClearSelectedOption}
                />
            </div>
        );
    }
}
